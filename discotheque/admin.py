from django.contrib import admin

# Register your models here.
from .models import Groupe, Album, Support, Album_support

admin.site.register(Groupe)
admin.site.register(Album)
admin.site.register(Support)
admin.site.register(Album_support)
