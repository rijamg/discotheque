# Generated by Django 3.1.5 on 2021-02-01 13:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discotheque', '0006_remove_album_id_groupe'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Album',
        ),
    ]
