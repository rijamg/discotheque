from django.apps import AppConfig


class DiscothequeConfig(AppConfig):
    name = 'discotheque'
