from django.shortcuts import render
from django.template import loader

from django.http import HttpResponse

from .models import Groupe
from .models import Album
from .models import Support
from .models import Album_support


def index(request):
    groupe_list = Groupe.objects.order_by('nom_groupe')
    context = {
        'groupe_list': groupe_list.all(),
    }
    return render(request, 'discotheque/index.html', context)
# Create your views here.

def detail(request, groupe_id):
    album_list = Album.objects.filter(id_groupe = groupe_id)
    group = Groupe.objects.get(pk = groupe_id)
    support_list = Support.objects.all()
    album_support_list = Album_support.objects.all()
    context = {
        'group' : group, 
        'album_list': album_list,
        'support_list' : support_list,
        'album_support_list': album_support_list,
    }
    return render(request, 'discotheque/album.html', context)

# def results(request, question_id):
#     response = "You're looking at the results of question %s."
#     return HttpResponse(response % question_id)

# def vote(request, question_id):
#     return HttpResponse("You're voting on question %s." % question_id)