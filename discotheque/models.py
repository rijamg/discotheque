from django.db import models
from django.db.models.deletion import CASCADE


class Groupe(models.Model):
    nom_groupe = models.CharField(max_length=200)
    groupe_image = models.ImageField(upload_to='media')
    
    def __str__(self):
        return self.nom_groupe

class Album(models.Model):
    nom_album = models.CharField(max_length=200)
    id_groupe = models.ForeignKey(Groupe, models.SET_NULL, blank=True, null=True,)

    def __str__(self):
        return self.nom_album    

class Support(models.Model):
    type_support = models.CharField(max_length=200)

    def __str__(self):
        return self.type_support

class Album_support(models.Model):
    id_album = models.ForeignKey(Album, models.SET_NULL, blank=True, null=True,)        
    id_support = models.ForeignKey(Support, models.SET_NULL, blank=True, null=True,)        
    